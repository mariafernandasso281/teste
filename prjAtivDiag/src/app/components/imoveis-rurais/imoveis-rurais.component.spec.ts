import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImoveisRuraisComponent } from './imoveis-rurais.component';

describe('ImoveisRuraisComponent', () => {
  let component: ImoveisRuraisComponent;
  let fixture: ComponentFixture<ImoveisRuraisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImoveisRuraisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImoveisRuraisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
