import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { DestaquesComponent } from './destaques/destaques.component';
import { FiltroComponent } from './filtro/filtro.component';



@NgModule({
  declarations: [
    HomeComponent,
    DestaquesComponent,
    FiltroComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HomeModule { }
