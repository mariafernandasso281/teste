import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeModule } from './components/home/home.module';
import { CadImovelModule } from './components/cad-imovel/cad-imovel.module';
import { ImoveisRuraisModule } from './components/imoveis-rurais/imoveis-rurais.module';
import { ImoveisUrbanosModule } from './components/imoveis-urbanos/imoveis-urbanos.module';
import { TopoComponent } from './topo/topo.component';

@NgModule({
  declarations: [
    AppComponent,
    TopoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    CadImovelModule,
    ImoveisRuraisModule,
    ImoveisUrbanosModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
